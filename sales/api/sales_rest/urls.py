from django.urls import path

from .views import (
    api_salespersons,
    api_customers,
    api_sale_records,
    api_unsold_automobiles,
)

urlpatterns = [
    path(
        "salespersons/",
        api_salespersons,
        name="api_salespersons",
    ),
        path(
        "customers/",
        api_customers,
        name="api_customers",
    ),
    path(
        "salerecords/",
        api_sale_records,
        name="api_sale_records",
    ),
    path(
        "automobiles/unsold/", 
        api_unsold_automobiles, 
        name="api_unsold_automobiles"
    ),
]
