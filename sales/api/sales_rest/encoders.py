from common.json import ModelEncoder

from .models import AutomobileVO, Salesperson, Customer, SaleRecord

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "id",
        "year",
        "color",
        "vin",
    ]

class SalespersonEncoder(ModelEncoder):
    model = Salesperson
    properties = [
        "id",
        "name",
        "employee_id",
    ]


class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = [
        "id",
        "name",
        "address",
        "phone_number",
    ]

class SaleRecordEncoder(ModelEncoder):
    model = SaleRecord
    properties = [
        "id",
        "price"
    ]
    def get_extra_data(self, o):
        return {
            "year": o.automobile.year,
            "color": o.automobile.color,
            "vin": o.automobile.vin,
            "employee": o.salesperson.name,
            "employee_id": o.salesperson.employee_id,
            "customer": o.customer.name,
        }
    encoders = {
        "automobile": AutomobileVOEncoder(),
        "employee_id": SalespersonEncoder(),
        "customer_id": CustomerEncoder(),
    }
