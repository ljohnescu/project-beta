import React from "react";
import { Link } from "react-router-dom";

class SaleRecordsList extends React.Component {
  constructor() {
    super();
    this.state = { sale_records: [] };
  }

  async componentDidMount() {
    const response = await fetch("http://localhost:8090/api/salerecords/");
    if (response.ok) {
      const data = await response.json();
      this.setState({ sale_records: data.sale_records });
    } else {
      console.error(response);
    }
  }

  render() {
    return (
        <div className="container">
          <h1>Sale history</h1>
          <div>
            <Link
              to="/sales/salerecords/new"
              className="btn btn-primary btn-md px-4 gap-3"
            >
              Record a new sale
            </Link>
          </div>
          <table className="table table-striped table-hover">
            <thead>
              <tr>
                <th>Salesperson</th>
                <th>Employee ID</th>
                <th>Customer</th>
                <th>VIN</th>
                <th>Sale price</th>
              </tr>
            </thead>
            <tbody>
              {this.state.sale_records.map((sale_record) => {
                return (
                  <tr key={sale_record.id}>
                    <td>{sale_record.employee}</td>
                    <td>{sale_record.employee_id}</td>
                    <td>{sale_record.customer}</td>
                    <td>{sale_record.vin}</td>
                    <td>{"$" + sale_record.price}</td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
    );
  }
}

export default SaleRecordsList;
