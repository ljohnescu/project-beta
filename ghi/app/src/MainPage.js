import { renderMatches } from "react-router-dom";

function MainPage() {
  const background = {
    backgroundImage: `url(${"https://images.unsplash.com/photo-1526726538690-5cbf956ae2fd?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=2370&q=80"})`,
    margin: "0 !important",
    backgroundRepeat: "no-repeat",
    opacity: 1,
    height: "100vh",
    opacity: 1,
  };
  const text = {
    color: "black",
  };

  return (
    <div style={background}>
      <div className="px-4 py-5 text-center">
        <h1 style={text} className="display-5 fw-bold color-primary">
          CarCar
        </h1>
        <div className="col-lg-6 mx-auto">
          <p style={text} className="lead mb-4">
            The premiere solution for automobile dealership management!
          </p>
        </div>
      </div>
    </div>
  );
}

export default MainPage;
