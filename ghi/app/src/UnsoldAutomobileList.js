import React from "react";

class UnsoldAutomobilesList extends React.Component {
  constructor() {
    super();
    this.state = { automobiles: [] };
  }

  async componentDidMount() {
    const response = await fetch(
      "http://localhost:8090/api/automobiles/unsold/"
    );
    if (response.ok) {
      const data = await response.json();
      this.setState({ automobiles: data.automobiles });
    }
  }

  render() {
    return (
        <div className="container">
          <h1>Current inventory</h1>
          <table className="table table-striped table-hover">
            <thead>
              <tr>
                <th>VIN</th>
              </tr>
            </thead>
            <tbody>
              {this.state.automobiles.map((automobile) => {
                return (
                  <tr key={automobile.id}>
                    <td>{automobile.vin}</td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
    );
  }
}

export default UnsoldAutomobilesList;
