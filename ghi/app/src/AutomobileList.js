import React from "react";
import { Link } from "react-router-dom";

class AutomobilesList extends React.Component {
  constructor() {
    super();
    this.state = { automobiles: [] };
  }

  async componentDidMount() {
    const response = await fetch("http://localhost:8100/api/automobiles/");
    if (response.ok) {
      const data = await response.json();
      this.setState({ automobiles: data.automobiles });
    } else {
      console.error(response);
    }
  }

  render() {
    return (
        <div className="container">
          <h1>Automobiles</h1>
          <div>
            <Link
              to="/inventory/automobiles/new"
              className="btn btn-primary btn-md px-4 gap-3"
            >
              Add an automobile
            </Link>
          </div>
          <table className="table table-striped table-hover">
            <thead>
              <tr>
                <th>VIN</th>
                <th>Color</th>
                <th>Year</th>
                <th>Model</th>
                <th>Manufacturer</th>
              </tr>
            </thead>
            <tbody>
              {this.state.automobiles.map((automobile) => {
                return (
                  <tr key={automobile.id}>
                    <td>{automobile.vin}</td>
                    <td>{automobile.color}</td>
                    <td>{automobile.year}</td>
                    <td>{automobile.model.name}</td>
                    <td>{automobile.model.manufacturer.name}</td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
    );
  }
}

export default AutomobilesList;
