import React from "react";
import { Link } from "react-router-dom";

class VehicleModelsList extends React.Component {
  constructor() {
    super();
    this.state = { models: [] };
  }

  async componentDidMount() {
    const response = await fetch("http://localhost:8100/api/models/");
    if (response.ok) {
      const data = await response.json();
      this.setState({ models: data.models });
    } else {
      console.error(response);
    }
  }

  render() {
    return (
        <div className="container">
          <h1>Models</h1>
          <div>
            <Link
              to="/inventory/models/new"
              className="btn btn-primary btn-md px-4 gap-3"
            >
              Add a model
            </Link>
          </div>
          <table className="table table-striped table-hover">
            <thead>
              <tr>
                <th>Name</th>
                <th>Manufacturer</th>
                <th>Picture</th>
              </tr>
            </thead>
            <tbody>
              {this.state.models.map((model) => {
                return (
                  <tr key={model.id}>
                    <td>{model.name}</td>
                    <td>{model.manufacturer.name}</td>
                    <td>
                      <img src={model.picture_url} width="200px" />
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
    );
  }
}

export default VehicleModelsList;
