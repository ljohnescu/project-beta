import { NavLink } from "react-router-dom";

function Nav() {
  const pad = {
    paddingRight: 4,
  };
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">
          <img
            style={pad}
            src="https://i.imgur.com/Q3OVzYQ.png"
            alt="Logo"
            width="30"
            height="24"
            className="d-inline-block align-text-top"
          />
          CarCar
        </NavLink>
        <button
          className="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item dropdown">
              <NavLink
                to="#"
                className="nav-link dropdown-toggle"
                role="button"
                id="dropdownMenuButton"
                data-bs-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"
              >
                Inventory
              </NavLink>
              <div
                className="dropdown-menu"
                aria-labelledby="dropdownMenuButton"
              >
                <NavLink
                  className="dropdown-item"
                  to="inventory/manufacturers/list"
                >
                  Manufacturers
                </NavLink>
                <NavLink className="dropdown-item" to="inventory/models/list">
                  Models
                </NavLink>
                <NavLink
                  className="dropdown-item"
                  to="inventory/automobiles/all"
                >
                  All automobiles
                </NavLink>
                <NavLink
                  className="dropdown-item"
                  to="inventory/automobiles/unsold"
                >
                  Current inventory
                </NavLink>
              </div>
            </li>
            <li className="nav-item dropdown">
              <NavLink
                to="#"
                className="nav-link dropdown-toggle"
                role="button"
                id="dropdownMenuButton"
                data-bs-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"
              >
                Sales
              </NavLink>
              <div
                className="dropdown-menu"
                aria-labelledby="dropdownMenuButton"
              >
                <NavLink className="dropdown-item" to="sales/customers/new">
                  New customer
                </NavLink>
                <NavLink className="dropdown-item" to="sales/salerecords/new">
                  New sale
                </NavLink>
                <NavLink className="dropdown-item" to="sales/salerecords/list">
                  All sale history
                </NavLink>
                <NavLink
                  className="dropdown-item"
                  to="sales/salerecords/filter"
                >
                  Sale history by salesperson
                </NavLink>
              </div>
            </li>

            <li className="nav-item dropdown">
              <NavLink
                to="#"
                className="nav-link dropdown-toggle"
                role="button"
                id="dropdownMenuButton"
                data-bs-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"
              >
                Service
              </NavLink>
              <div
                className="dropdown-menu"
                aria-labelledby="dropdownMenuButton"
              >
                <NavLink
                  className="dropdown-item"
                  to="service/appointments/new"
                >
                  New appointment
                </NavLink>
                <NavLink
                  className="dropdown-item"
                  to="service/appointments/list"
                >
                  Upcoming appointments
                </NavLink>
                <NavLink
                  className="dropdown-item"
                  to="service/appointments/history"
                >
                  Appointment history
                </NavLink>
              </div>
            </li>

            <li className="nav-item dropdown">
              <NavLink
                to="#"
                className="nav-link dropdown-toggle"
                role="button"
                id="dropdownMenuButton"
                data-bs-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"
              >
                Personnel
              </NavLink>
              <div
                className="dropdown-menu"
                aria-labelledby="dropdownMenuButton"
              >
                <NavLink className="dropdown-item" to="personnel/list">
                  Personnel list
                </NavLink>
                <NavLink
                  className="dropdown-item"
                  to="personnel/salespersons/new"
                >
                  New salesperson
                </NavLink>
                <NavLink
                  className="dropdown-item"
                  to="personnel/technicians/new"
                >
                  New technician
                </NavLink>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  );
}

export default Nav;
