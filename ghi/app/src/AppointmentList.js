import React from "react";
import { Link } from "react-router-dom";

class AppointmentList extends React.Component {
  constructor() {
    super();
    this.state = { appointments: [] };
    this.deleteAppointment = this.deleteAppointment.bind(this);
  }

  async componentDidMount() {
    const response = await fetch("http://localhost:8080/api/appointments/");
    if (response.ok) {
      const data = await response.json();
      const filterData = data.appointments.filter(
        (appointment) => appointment.completed === false
      );
      this.setState({ appointments: filterData });
    } else {
      console.error(response);
    }
  }

  async deleteAppointment(event) {
    const url = `http://localhost:8080/api/appointments/${event.target.value}`;
    const fetchConfig = {
      method: "delete",
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(url, fetchConfig);
    if (response.ok) {
      this.setState({
        appointments: this.state.appointments.filter(
          (appointment) => appointment.id != event.target.value
        ),
      });
    } else {
      throw new Error("Error: response not okay");
    }
  }

  async completeAppointment(event) {
    event.preventDefault();
    const data = { ...this.state };

    const appUrl = `http://localhost:8080/api/appointments/${event.target.value}/`;
    const fetchConfig = {
      method: "put",
      body: JSON.stringify({ completed: true }),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(appUrl, fetchConfig);
    this.componentDidMount();
  }

  render() {
    return (
      <React.Fragment>
        <div className="container">
          <div>
            <h1>Upcoming appointments</h1>
          </div>
          <div>
            <Link
              to="/service/appointments/new"
              className="btn btn-primary btn-md px-4 gap-3"
            >
              Create an appointment
            </Link>
          </div>
          <table className="table table-striped table-hover">
            <thead>
              <tr>
                <th>VIN</th>
                <th>Name</th>
                <th>Date</th>
                <th>Time</th>
                <th>VIP</th>
                <th>Reason</th>
                <th>Technician</th>
              </tr>
            </thead>
            <tbody>
              {this.state.appointments.map((appointment) => {
                return (
                  <tr key={appointment.id}>
                    <td>{appointment.vin}</td>
                    <td>{appointment.name}</td>
                    <td>{appointment.date}</td>
                    <td>{appointment.time}</td>
                    {appointment.vip ? (
                      <td>
                        <span role="img" aria-label="check">
                          👑
                        </span>
                      </td>
                    ) : (
                      <td>
                        <span role="img" aria-label="x">
                          🙅
                        </span>
                      </td>
                    )}
                    <td>{appointment.reason}</td>
                    <td>{appointment.tech_name}</td>
                    <td>
                      <button
                        className="btn btn-sm btn-success"
                        value={appointment.id}
                        onClick={this.completeAppointment.bind(this)}
                      >
                        Complete
                      </button>
                    </td>
                    <td>
                      <button
                        className="btn btn-sm btn-danger"
                        value={appointment.id}
                        onClick={this.deleteAppointment.bind(this)}
                      >
                        Delete
                      </button>
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
      </React.Fragment>
    );
  }
}

export default AppointmentList;
