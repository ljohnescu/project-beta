import React from "react";

class SalespersonForm extends React.Component {
  constructor() {
    super();
    this.state = {
      name: "",
      employee_id: "",
    };
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleInputChange(event) {
    const { value, name } = event.target;
    this.setState({ [name]: value });
  }

  async handleSubmit(event) {
    event.preventDefault();
    const data = { ...this.state };

    const salespersonUrl = "http://localhost:8090/api/salespersons/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(salespersonUrl, fetchConfig);
    if (response.ok) {
      window.location.href = "/personnel/list";
    }
  }

  render() {
    return (
      <div className="container">
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Add a new salesperson</h1>
              <form onSubmit={this.handleSubmit} id="create-employee-form">
                <div className="form-floating mb-3">
                  <input
                    onChange={this.handleInputChange}
                    value={this.state.name}
                    placeholder="Salesperson name"
                    required
                    type="text"
                    name="name"
                    id="name"
                    className="form-control"
                  />
                  <label htmlFor="name">Salesperson name</label>
                </div>
                <div className="form-floating mb-3">
                  <input
                    onChange={this.handleInputChange}
                    value={this.state.employee_id}
                    placeholder="Employee ID"
                    required
                    type="text"
                    name="employee_id"
                    id="employee_id"
                    className="form-control"
                  />
                  <label htmlFor="employee_id">Employee ID</label>
                </div>
                <button className="btn btn-success">Create</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default SalespersonForm;
